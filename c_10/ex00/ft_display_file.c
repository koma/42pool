/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 21:04:13 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/14 13:55:44 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <unistd.h>

int		main(int argc, char **argv)
{
	int		fd;
	char	buf[2];
	int		offset;

	if (argc == 1)
		write(2, "File name missing.\n", 20);
	else if (argc > 2)
		write(2, "Too many arguments.\n", 21);
	else
	{
		fd = open(argv[1], O_RDONLY);
		if (read(fd, buf, 1) >= 0)
		{
			buf[1] = 0;
			close(fd);
			fd = open(argv[1], O_RDONLY);
			while ((offset = read(fd, buf, 1)))
				write(1, buf, 1);
			close(fd);
		}
		else
			write(2, "Cannot read file.\n", 19);
	}
	return (0);
}
