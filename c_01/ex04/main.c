#include <stdio.h>

void	ft_ultimate_div_mod(int *a, int *b);

int main()
{
	int a;
	int b;

	a = 109;
	b = 5;

	ft_ultimate_div_mod(&a, &b);
	printf("%d\n%d\n", a, b);
}
