#include <stdio.h>

void	ft_div_mod(int a, int b, int *div, int *mod);

int main()
{
	int a;
	int b;
	int div;
	int mod;

	a = 53;
	b = 10;

	ft_div_mod(a, b, &div, &mod);
	printf("%d\n%d\n", div, mod);
}
