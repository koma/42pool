#include <stdio.h>

void	ft_rev_int_tab(int *tab, int size);

int main()
{
	int tab[9] = {19, -1, -2147483648, 2147483647, 5, 3, 7, 8, 9};
	int i = 0;
	while (i <= 8)
	{
		printf("%d.", tab[i]);
		i++;
	}

	ft_rev_int_tab(tab, 9);
	i = 0;
	printf("\n");
	while (i <= 8)
	{
		printf("%d.", tab[i]);
		i++;
	}
}
