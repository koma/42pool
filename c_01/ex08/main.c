#include <stdio.h>
void	ft_sort_int_tab(int *tab, int size);

int main()
{
	int tab[9] = {9, 2147483647, -2147483648, 9, 9, 9, 9, 8, 9};
	int i = 0;
	while (i < 9)
	{
		printf("%d.", tab[i]);
		i++;
	}

	ft_sort_int_tab(tab, 9);
	i = 0;
	printf("\n");
	while (i < 9)
	{
		printf("%d.", tab[i]);
		i++;
	}
}
