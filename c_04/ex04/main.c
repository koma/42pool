#include <limits.h>

void	ft_putnbr_base(int nbr, char *base);

int	main()
{
	int nbr = 42;
	char base[] = "012A3456789ABCDF";
	ft_putnbr_base(nbr, base);
}
