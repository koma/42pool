/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 14:58:42 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/05 12:07:18 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr_core(int nb)
{
	int		i;
	long	pow10;
	long	tmp;

	i = 0;
	pow10 = 1;
	tmp = nb;
	while (tmp)
	{
		tmp /= 10;
		i++;
		pow10 *= 10;
	}
	while (nb)
	{
		pow10 /= 10;
		tmp += (nb % 10) * pow10;
		nb /= 10;
	}
	while (i-- > 0)
	{
		nb = tmp % 10 + 48;
		tmp /= 10;
		write(1, &nb, 1);
	}
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
	{
		write(1, "-2147483648", 11);
		return ;
	}
	if (nb == 0)
	{
		write(1, "0", 1);
		return ;
	}
	if (nb < 0)
	{
		write(1, "-", 1);
		nb *= -1;
	}
	ft_putnbr_core(nb);
}
