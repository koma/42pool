#include <stdlib.h>
#include <stdio.h>

int	ft_atoi(char *str);

int main()
{
	char str[] = "-2147483648";
	printf("%d\n", ft_atoi(str));
	printf("%d\n", atoi(str));
}
