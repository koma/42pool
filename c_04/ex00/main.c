#include <stdio.h>
#include <string.h>

int ft_strlen(char *str);

int main()
{
	char str[] = "abcde";
	printf("%d\n", ft_strlen(str));
	printf("%lu\n", strlen(str));
}
