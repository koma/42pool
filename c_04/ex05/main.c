#include <stdio.h>

int	ft_atoi_base(char *str, char *base);

int	main()
{
	char str[] = "  \n\r\t  \v \f       +----10000000000000000000000000000000";
	char base[] = "01";
	printf("%d", ft_atoi_base(str, base));
}
