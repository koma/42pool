/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 15:18:23 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/05 17:06:00 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_atoi_base_core(char *str, char *base, int *nb, int i)
{
	int k;
	int l;
	int sign;

	k = 0;
	sign = 1;
	while (str[k] == ' ' || (str[k] >= '\t' && str[k] <= '\r'))
		k++;
	while (str[k] == '-' || str[k] == '+')
	{
		if (str[k++] == '-')
			sign *= -1;
	}
	while (str[k])
	{
		l = 0;
		while (str[k] != base[l] && base[l])
			l++;
		if (base[l])
			*nb = *nb * i + l;
		else
			break ;
		k++;
	}
	*nb *= sign;
}

int		ft_atoi_base(char *str, char *base)
{
	int i;
	int j;
	int nb;

	i = 0;
	nb = 0;
	while (base[i])
	{
		if (base[i] == '-' || base[i] == '+'
				|| base[i] == ' ' || (base[i] >= '\t' && base[i] <= '\r'))
			return (0);
		j = i + 1;
		while (base[j])
		{
			if (base[i] == base[j++])
				return (0);
		}
		i++;
	}
	if (i == 0 || i == 1)
		return (0);
	ft_atoi_base_core(str, base, &nb, i);
	return (nb);
}
