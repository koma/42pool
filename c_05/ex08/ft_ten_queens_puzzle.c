/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ten_queens_puzzle.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 20:10:18 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/08 23:55:23 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	check(int ten[10], int k, int i)
{
	int j;

	j = 0;
	while (j < i)
	{
		if (k == ten[j] || k == ten[j] - i + j || k == ten[j] + i - j)
			return (1);
		j++;
	}
	return (0);
}

int	bt(int ten[10], int i, int *c)
{
	int k;

	if (i == 10)
	{
		i = 0;
		while (i < 10)
		{
			k = ten[i++] + 48;
			write(1, &k, 1);
		}
		(*c)++;
		write(1, "\n", 1);
		return (0);
	}
	k = 0;
	while (k < 10)
	{
		if (check(ten, k, i) == 0)
		{
			ten[i] = k;
			bt(ten, i + 1, c);
		}
		k++;
	}
	return (0);
}

int	ft_ten_queens_puzzle(void)
{
	int ten[10];
	int c;

	c = 0;
	bt(ten, 0, &c);
	return (c);
}
