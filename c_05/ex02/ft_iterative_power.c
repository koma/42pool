/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/05 13:18:26 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/05 15:37:34 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int i;
	int nb_orig;

	if (power == 0)
		return (1);
	if (power < 0)
		return (0);
	i = 1;
	nb_orig = nb;
	while (i++ < power)
		nb *= nb_orig;
	return (nb);
}
