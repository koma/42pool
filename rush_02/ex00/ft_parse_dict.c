/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 15:33:51 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/15 23:07:29 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		ft_strncpy(char *src, int k, char **dest, char *lim)
{
	int l;
	int m;
	int o;

	m = k;
	l = 0;
	while (src[k] != lim[0] && src[k] != lim[1])
	{
		l++;
		k++;
	}
	if (!(*dest = (char *)malloc(sizeof(**dest) * (l + 1))))
		return (0);
	k = m;
	o = 0;
	while (k - m < l)
		(*dest)[o++] = src[k++];
	(*dest)[l] = 0;
	return (k);
}

void	ft_struct_init(char *dict, t_dict **numbers)
{
	int		i;
	int		k;

	i = 0;
	k = -1;
	while (dict[++k])
	{
		if (dict[k] == '\n' && dict[k + 1] != '\n')
			i++;
	}
	if (!(*numbers = (t_dict *)malloc(sizeof(**numbers) * (i + 1))))
		return ;
	i = -1;
	k = -1;
	while (++i > -1 && dict[++k])
	{
		while (dict[k] == '\n')
			k++;
		k = ft_strncpy(dict, k, &(*numbers)[i].nb, ": ");
		while (dict[k] == ' ' || dict[k] == ':')
			k++;
		k = ft_strncpy(dict, k, &(*numbers)[i].nb_en, "\n\n");
		(*numbers)[i].nblen = ft_strlen((*numbers)[i].nb);
	}
	(*numbers)[i].nblen = 0;
}

int		ft_check_dict(char *dict)
{
	int i;

	i = 0;
	while (dict[i])
	{
		while (dict[i] == '\n')
			i++;
		while (dict[i] >= '0' && dict[i] <= '9')
			i++;
		while (dict[i] == ' ')
			i++;
		if (dict[i] == ':')
			i++;
		else
			return (1);
		while (dict[i] == ' ')
			i++;
		while (dict[i] >= ' ' && dict[i] <= '~')
			i++;
		if (dict[i] == '\n')
			i++;
		else
			return (1);
	}
	return (0);
}

char	*ft_read_dict(char *file)
{
	int		fd;
	char	buf[2];
	char	*dict;
	int		i;

	fd = open(file, O_RDONLY);
	buf[1] = 0;
	i = 0;
	while ((read(fd, buf, 1)))
		i++;
	close(fd);
	if (!(dict = (char *)malloc(sizeof(*dict) * (i + 1))))
		return (NULL);
	dict[i] = 0;
	fd = open(file, O_RDONLY);
	i = 0;
	while ((read(fd, buf, 1)))
		dict[i++] = buf[0];
	close(fd);
	return (dict);
}
