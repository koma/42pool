/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 16:02:32 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/15 23:42:07 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H

# include <sys/types.h>
# include <sys/stat.h>
# include <sys/uio.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>

typedef	struct	s_dict
{
	char	*nb;
	int		nblen;
	char	*nb_en;
}				t_dict;

int				ft_strlen(char *str);
void			ft_putstr(char *str);
int				ft_init(char *file, char *nbr);

int				ft_strncpy(char *src, int k, char **dest, char *lim);
int				ft_check_dict(char *dict);
void			ft_struct_init(char *dict, t_dict **numbers);
char			*ft_read_dict(char *file);

void			ft_put_unit(char d, t_dict *dict);
void			ft_put_tens(char *dd, t_dict *dict);
void			ft_put_hundreds(char d, t_dict *dict);
void			ft_put_thousands(int size, t_dict *dict);

int				ft_check_zeros(char *nbr, int v);
char			*ft_thou_zeros(char *nbr, int *size, t_dict *dict);
char			*ft_put_nonzero(char *nbr, int *size, t_dict *dict);
char			*ft_skip_zeros(char *nbr, int *size);
void			ft_parse_nbr(char *nbr, t_dict *dict);

#endif
