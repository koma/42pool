/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_en.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/15 22:56:04 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/15 22:56:45 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	ft_put_unit(char d, t_dict *dict)
{
	int i;

	i = 0;
	while (dict[i].nblen)
	{
		if (dict[i].nblen == 1 && d == dict[i].nb[0])
		{
			ft_putstr(dict[i].nb_en);
			return ;
		}
		i++;
	}
}

void	ft_put_tens(char *dd, t_dict *dict)
{
	int i;

	i = 0;
	while (dict[i].nblen)
	{
		if (dd[0] > '1' && dict[i].nblen == 2
				&& dd[0] == dict[i].nb[0] && dict[i].nb[1] == '0')
		{
			ft_putstr(dict[i].nb_en);
			return ;
		}
		else if (dd[0] == '1' && dict[i].nblen == 2
				&& dict[i].nb[0] == '1' && dd[1] == dict[i].nb[1])
		{
			ft_putstr(dict[i].nb_en);
			return ;
		}
		i++;
	}
}

void	ft_put_hundreds(char d, t_dict *dict)
{
	int i;

	ft_put_unit(d, dict);
	write(1, " ", 1);
	i = 0;
	while (dict[i].nblen)
	{
		if (dict[i].nblen == 3 && ft_check_zeros(dict[i].nb, 0) == 0)
		{
			ft_putstr(dict[i].nb_en);
			return ;
		}
		i++;
	}
}

void	ft_put_thousands(int size, t_dict *dict)
{
	int i;

	i = 0;
	while (dict[i].nblen)
	{
		if (dict[i].nblen == size && ft_check_zeros(dict[i].nb, 0) == 0)
		{
			ft_putstr(dict[i].nb_en);
			return ;
		}
		i++;
	}
}
