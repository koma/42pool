/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/15 22:37:49 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/15 22:59:47 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
			i++;
		else
			return (-1);
	}
	return (i);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
		write(1, &str[i++], 1);
}

int		ft_init(char *file, char *nbr)
{
	char	*dict;
	t_dict	*numbers;
	int		i;

	dict = ft_read_dict(file);
	if (ft_check_dict(dict) == 0)
	{
		ft_struct_init(dict, &numbers);
		free(dict);
		ft_parse_nbr(nbr, numbers);
	}
	else
	{
		free(dict);
		write(2, "Dict Error\n", 11);
		return (1);
	}
	i = -1;
	while (numbers[++i].nblen)
	{
		free(numbers[i].nb);
		free(numbers[i].nb_en);
	}
	free(numbers);
	return (0);
}

int		main(int argc, char **argv)
{
	if (argc == 2)
		ft_init("numbers.dict", argv[1]);
	else if (argc == 3)
		ft_init(argv[1], argv[2]);
	else
	{
		write(2, "Error\n", 6);
		return (1);
	}
	return (0);
}
