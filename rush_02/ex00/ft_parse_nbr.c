/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_nbr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 22:31:34 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/15 23:40:33 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		ft_check_zeros(char *nbr, int v)
{
	int i;

	i = 1;
	if (v == 0)
	{
		if (nbr[0] != '1')
			return (1);
	}
	while (nbr[i])
	{
		if (nbr[i] != '0')
			return (1);
		i++;
	}
	return (0);
}

char	*ft_thou_zeros(char *nbr, int *size, t_dict *dict)
{
	write(1, " ", 1);
	ft_put_thousands(*size, dict);
	if (ft_check_zeros(nbr, 1) == 0)
	{
		while (*(nbr + 1))
			nbr++;
		*size = 1;
	}
	return (nbr);
}

char	*ft_put_nonzero(char *nbr, int *size, t_dict *dict)
{
	if (*size % 3 == 0)
		ft_put_hundreds(nbr[0], dict);
	else if (*size % 3 == 2)
	{
		ft_put_tens(nbr, dict);
		if (nbr[0] == '1' || nbr[1] == '0')
		{
			(*size)--;
			nbr++;
			if ((*size - 1) / 3 > 0)
				nbr = ft_thou_zeros(nbr, size, dict);
		}
	}
	else if (*size % 3 == 1)
	{
		if (nbr[0] != '0')
			ft_put_unit(nbr[0], dict);
		if (*size / 3 > 0)
			nbr = ft_thou_zeros(nbr, size, dict);
	}
	return (nbr);
}

char	*ft_skip_zeros(char *nbr, int *size)
{
	while (*nbr == '0' && *size > 1)
	{
		nbr++;
		(*size)--;
	}
	return (nbr);
}

void	ft_parse_nbr(char *nbr, t_dict *dict)
{
	int		size;

	size = ft_strlen(nbr);
	if (size > 39 || size <= 0)
	{
		write(2, "Error\n", 6);
		return ;
	}
	nbr = ft_skip_zeros(nbr, &size);
	if (nbr[0] == '0' && size == 1)
		ft_put_unit(nbr[0], dict);
	while (*nbr && size)
	{
		if (nbr[0] != '0' || size % 3 == 1)
			nbr = ft_put_nonzero(nbr, &size, dict);
		if (size > 1 && nbr[1] != '0')
			write(1, " ", 1);
		else if (!(*(nbr + 1)) && size == 1)
			write(1, "\n", 1);
		size--;
		nbr++;
	}
}
