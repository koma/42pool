#include <stdio.h>
#include <string.h>
#include <stdlib.h>

unsigned int ft_strlcpy(char *dest, char *src, unsigned int size);

int main(int ac, char *av[])
{
	char dest[] = "abce";
	printf("%u\n", ft_strlcpy(dest, av[2], atoi(av[3])));
	printf("%s\n", dest);
	printf("%lu\n", strlcpy(av[1], av[2], atoi(av[3])));
	printf("%s\n", av[1]);
	return (ac);
}
