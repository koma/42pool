/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 14:41:54 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/04 12:05:24 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr_hex(long nb)
{
	char c;

	if (nb / 16 > 0)
		ft_putnbr_hex(nb / 16);
	if (nb % 16 > 9)
		c = nb % 16 + 87;
	else
		c = nb % 16 + 48;
	write(1, &c, 1);
}

void	ft_puthex_two_bytes(long a, unsigned int *c, int *j, int l)
{
	int	o;

	o = 0;
	while (o < 2)
	{
		if (*j < l)
		{
			c[*j] = (*(unsigned char*)(a + c[16] * 16 + *j));
			if ((long)c[*j] < 16)
				write(1, "0", 1);
			ft_putnbr_hex((long)c[*j]);
		}
		else
			write(1, "  ", 2);
		(*j)++;
		o++;
	}
	write(1, " ", 1);
}

void	ft_print_data(long a, unsigned int *c, unsigned int s)
{
	int	j;
	int	l;

	j = 0;
	l = 16;
	if (c[16] + 1 > s / 16)
		l = s % 16;
	while (j < 16)
	{
		ft_puthex_two_bytes(a, c, &j, l);
	}
	j = 0;
	while (j < l)
	{
		if (c[j] < 32 || c[j] > 126)
			write(1, ".", 1);
		else
			write(1, &(c[j]), 1);
		j++;
	}
}

void	ft_print_zero(long a, unsigned int i)
{
	int		j;
	long	b;

	j = 0;
	b = 1;
	while (b <= a + 16 * i)
	{
		j++;
		b *= 16;
	}
	b = 0;
	while (b++ < 15 - j)
		write(1, "0", 1);
}

void	*ft_print_memory(void *addr, unsigned int size)
{
	unsigned int	i;
	unsigned int	k;
	unsigned int	c[17];

	i = 0;
	k = size / 16;
	if (size % 16 != 0)
		k++;
	while (i < k)
	{
		ft_print_zero((long)addr, i);
		ft_putnbr_hex((long)addr + 16 * i);
		write(1, ": ", 2);
		c[16] = i;
		ft_print_data((long)addr, c, size);
		write(1, "\n", 1);
		i++;
	}
	return (addr);
}
