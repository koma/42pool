/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 23:08:45 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/03 23:59:01 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_puthex(int hex)
{
	if (hex > 9)
		hex += 87;
	else
		hex += 48;
	write(1, &hex, 1);
}

void	*ft_putstr_non_printable(char *str)
{
	int				i;
	unsigned char	c;
	int				hex;

	i = 0;
	while (str[i])
	{
		if ((str[i] < 32) || str[i] == 127)
		{
			c = str[i];
			write(1, "\\", 1);
			hex = c / 16;
			ft_puthex(hex);
			hex = c % 16;
			ft_puthex(hex);
		}
		else
			write(1, &(str[i]), 1);
		i++;
	}
	return (str);
}
