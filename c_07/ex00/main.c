#include <stdio.h>
#include <string.h>

char	*ft_strdup(char *src);

int main()
{
	char *str = "hello";
	printf("%s\n", str);
	printf("%p\n", str);
	char *str2 = strdup(str);
	printf("%s\n", str2);
	printf("%p\n", str2);
	char *str3 = ft_strdup(str);
	printf("%s\n", str3);
	printf("%p\n", str3);
}
