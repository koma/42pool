#include <stdio.h>

int	*ft_range(int min, int max);

int main()
{
	int min = 1;
	int max = 5;
	int i = 0;
	int *tab = ft_range(min, max);
	while (min < max)
	{
		printf("%d\n", tab[i]);
		min++;
		i++;
	}
}
