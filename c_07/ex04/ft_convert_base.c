/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 20:09:06 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/13 15:33:53 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_count_and_check_base(char *b);

char	*ft_strnbr_base(long nb, int *sign, char *b, int j)
{
	int		i;
	long	k;
	char	*nb_to;

	i = 0;
	k = 1;
	while (nb >= k)
	{
		k = k * j;
		i++;
	}
	if (*sign == -1)
		i++;
	if (!(nb_to = (char *)malloc(sizeof(*nb_to) * (i + 1))))
		return (NULL);
	nb_to[0] = 0;
	if (*sign == -1)
		nb_to[0] = '-';
	nb_to[i--] = 0;
	while (nb > 0)
	{
		nb_to[i--] = b[nb % j];
		nb /= j;
	}
	return (nb_to);
}

long	ft_atoi_base(char *nbr, int k, char *b, int i)
{
	long	nb;
	int		l;

	nb = 0;
	while (nbr[k])
	{
		l = 0;
		while (nbr[k] != b[l] && b[l])
			l++;
		if (b[l])
		{
			nb = nb * i + l;
		}
		else
			break ;
		k++;
	}
	return (nb);
}

long	ft_check_nbr(char *nbr, int *sign, char *b, int i)
{
	int	k;

	k = 0;
	*sign = 1;
	while (nbr[k] == ' ' || (nbr[k] >= '\t' && nbr[k] <= '\r'))
		k++;
	while (nbr[k] == '-' || nbr[k] == '+')
	{
		if (nbr[k++] == '-')
			*sign *= -1;
	}
	return (ft_atoi_base(nbr, k, b, i));
}

char	*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int		i;
	int		j;
	int		sign;
	long	nb;
	char	*nb_to0;

	i = ft_count_and_check_base(base_from);
	j = ft_count_and_check_base(base_to);
	if (i == 1 || j == 1)
		return (NULL);
	nb = ft_check_nbr(nbr, &sign, base_from, i);
	if (nb == 0)
	{
		if (!(nb_to0 = (char *)malloc(sizeof(*nb_to0) * 2)))
			return (NULL);
		nb_to0[0] = base_to[0];
		nb_to0[1] = 0;
		return (nb_to0);
	}
	return (ft_strnbr_base(nb, &sign, base_to, j));
}
