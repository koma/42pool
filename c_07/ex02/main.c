#include <stdio.h>

int	ft_ultimate_range(int **range, int min, int max);

int main()
{
	int min = 3;
	int max = 9;
	int *range;
	printf("taille de range = %d\n", ft_ultimate_range(&range, min, max));
	printf("%p\n", range);
	int i = 0;
	while (min < max)
	{
		printf("%d\n", range[i]);
		min++;
		i++;
	}

	//int arr[5];
	//int *p = arr;
	//int (*ptr)[5] = &arr;

	//printf("p = %p, ptr = %p\n", p, ptr);
	//printf("*p = %d, *ptr = %p\n", *p, *ptr);
	//printf("sizeof(p) = %lu, sizeof(*p) = %lu\n", sizeof(p), sizeof(*p));
	//printf("sizeof(ptr) = %lu, sizeof(*ptr) = %lu\n", sizeof(ptr), sizeof(*ptr));

	//printf("p = %p, ptr = %p\n", p, *ptr);
	//p++;
	//ptr++;
	//printf("p = %p, ptr = %p\n", p, ptr);
}
