/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 16:56:15 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/12 20:49:01 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strtablen(char **strs, int size)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	k = 0;
	while (i < size)
	{
		j = 0;
		while (strs[i][j])
		{
			j++;
			k++;
		}
		i++;
	}
	return (k);
}

char	*ft_malloc(char *str, int size, char **strs, char *sep)
{
	int		i;
	int		k;

	k = ft_strtablen(strs, size);
	i = 0;
	while (sep[i])
		i++;
	if (size == 0)
	{
		if (!(str = (char *)malloc(sizeof(**strs))))
			return (NULL);
		str[0] = 0;
		return (str);
	}
	if (!(str = (char *)malloc(sizeof(**strs) * k
					+ sizeof(*sep) * i * (size - 1) + 1)))
		return (NULL);
	str[0] = 0;
	return (str);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		i;
	int		j;
	int		k;
	char	*str_tmp;
	char	*str;

	str_tmp = 0;
	str = ft_malloc(str_tmp, size, strs, sep);
	i = 0;
	k = 0;
	while (i < size)
	{
		j = 0;
		while (strs[i][j])
			str[k++] = strs[i][j++];
		if (i != size - 1)
		{
			j = 0;
			while (sep[j])
				str[k++] = sep[j++];
		}
		i++;
	}
	str[k] = 0;
	return (str);
}
