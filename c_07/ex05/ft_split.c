/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 15:12:43 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/12 20:24:59 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	ft_strlcpy(char *dest, char *src, int size)
{
	int i;

	i = -1;
	while (src[++i] && i < size - 1)
	{
		dest[i] = src[i];
	}
	dest[i] = 0;
}

int		ft_wordlen(char *str, int i, char *charset, int v)
{
	int j;
	int k;

	k = 0;
	while (str[i])
	{
		j = 0;
		while (charset[j])
		{
			if (str[i] == charset[j])
			{
				if (v == 0)
					return (k);
				else
					return (i);
			}
			j++;
		}
		i++;
		k++;
	}
	if (v == 0)
		return (k);
	else
		return (i);
}

int		ft_wordcount(char *str, char *charset)
{
	int i;
	int j;
	int k;

	i = 0;
	k = 0;
	while (str[i])
	{
		j = ft_wordlen(str, i, charset, 1);
		if (j > i)
		{
			k++;
			i = j;
		}
		else
			i = j + 1;
	}
	return (k);
}

char	**ft_split(char *str, char *charset)
{
	int		i;
	int		j;
	int		k;
	int		l;
	char	**strs;

	j = ft_wordcount(str, charset);
	strs = (char **)malloc(sizeof(*strs) * (j + 1));
	i = 0;
	k = 0;
	while (k < j)
	{
		if (ft_wordlen(str, i, charset, 0) > 0)
		{
			l = i;
			i = ft_wordlen(str, l, charset, 1);
			strs[k] = (char *)malloc(sizeof(**strs) * (i - l + 1));
			strs[k][0] = 0;
			ft_strlcpy(strs[k++], str + l, i - l + 1);
		}
		else
			i++;
	}
	strs[j] = 0;
	return (strs);
}
