/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prototypes.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qufours <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 18:08:10 by qufours           #+#    #+#             */
/*   Updated: 2019/09/08 18:19:33 by qufours          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROTOTYPES_H
# define PROTOTYPES_H

int	valid(char res[4][4], int pos, char input[16]);
int	check_left(char input, char res[4][4], int i, int j);
int	check_top(char input, char res[4][4], int i, int j);
int	check_right(char input, char res[4][4], int i);
int	check_bottom(char input, char res[4][4], int j);

#endif
