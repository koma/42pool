/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush_check.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: madiarra <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 17:45:00 by madiarra          #+#    #+#             */
/*   Updated: 2019/09/08 17:53:31 by madiarra         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	check_left(char input, char res[4][4], int i, int j)
{
	int k;
	int l;
	int cpt;

	k = 0;
	l = 1;
	cpt = 1;
	while (l <= j)
	{
		if (res[i][k] < res[i][l])
		{
			k = l;
			cpt++;
		}
		l++;
	}
	if (j == 3 && cpt == input)
		return (0);
	else if (j != 3 && cpt <= input)
		return (0);
	else
		return (1);
}

int	check_top(char input, char res[4][4], int i, int j)
{
	int k;
	int l;
	int cpt;

	k = 0;
	l = 1;
	cpt = 1;
	while (l <= i)
	{
		if (res[k][j] < res[l][j])
		{
			k = l;
			cpt++;
		}
		l++;
	}
	if (i == 3 && cpt == input)
		return (0);
	else if (i != 3 && cpt <= input)
		return (0);
	else
		return (1);
}

int	check_right(char input, char res[4][4], int i)
{
	int k;
	int l;
	int cpt;

	k = 3;
	l = 2;
	cpt = 1;
	while (l >= 0)
	{
		if (res[i][k] < res[i][l])
		{
			k = l;
			cpt++;
		}
		l--;
	}
	if (cpt == input)
		return (0);
	else
		return (1);
}

int	check_bottom(char input, char res[4][4], int j)
{
	int k;
	int l;
	int cpt;

	k = 3;
	l = 2;
	cpt = 1;
	while (l >= 0)
	{
		if (res[k][j] < res[l][j])
		{
			k = l;
			cpt++;
		}
		l--;
	}
	if (cpt == input)
		return (0);
	else
		return (1);
}
