/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush_01.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 13:59:14 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/08 18:06:42 by qufours          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prototypes.h"

int	absent_line(int val, char res[4][4], int i, int j)
{
	int k;

	k = 0;
	while (k < 4)
	{
		if (k == j)
		{
			if (j == 3)
				return (0);
			else
				k++;
		}
		if (res[i][k] == val)
			return (1);
		else
			k++;
	}
	return (0);
}

int	absent_col(int val, char res[4][4], int i, int j)
{
	int k;

	k = 0;
	while (k < 4)
	{
		if (k == i)
		{
			if (i == 3)
				return (0);
			else
				k++;
		}
		if (res[k][j] == val)
			return (1);
		k++;
	}
	return (0);
}

int	check(int k, char input[16], char res[4][4], int pos)
{
	int i;
	int j;

	i = pos / 4;
	j = pos % 4;
	if (absent_line(k, res, i, j) == 0
			&& check_left(input[i + 8], res, i, j) == 0
			&& absent_col(k, res, i, j) == 0
			&& check_top(input[j], res, i, j) == 0)
	{
		if (i != 3 && j != 3)
			return (0);
		else if (j == 3 && i != 3 && check_right(input[i + 12], res, i) == 0)
			return (0);
		else if (i == 3 && j != 3 && check_bottom(input[j + 4], res, j) == 0)
			return (0);
		else if (i == 3 && j == 3 && check_right(input[i + 12], res, i) == 0
			&& check_bottom(input[j + 4], res, j) == 0)
			return (0);
		else
			return (1);
	}
	else
		return (1);
}

int	valid(char res[4][4], int pos, char input[16])
{
	int i;
	int j;
	int k;

	i = pos / 4;
	j = pos % 4;
	if (pos == 16)
		return (0);
	k = 1;
	while (k <= 4)
	{
		res[i][j] = k;
		if (check(k, input, res, pos) == 0)
		{
			if (valid(res, pos + 1, input) == 0)
				return (0);
		}
		k++;
	}
	res[i][j] = 0;
	return (1);
}
