/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qufours <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 16:24:52 by qufours           #+#    #+#             */
/*   Updated: 2019/09/08 18:06:23 by qufours          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "prototypes.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		check_argv(char *argv)
{
	int i;

	i = 0;
	while (argv[i])
	{
		if (i % 2 == 0 && (argv[i] < '1' || argv[i] > '4'))
			return (1);
		if (i % 2 != 0 && argv[i] != ' ')
			return (1);
		i++;
	}
	if (i != 31)
		return (1);
	return (0);
}

void	ft_print(char *res)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		if (i < 3)
		{
			ft_putchar(res[i] + '0');
			ft_putchar(' ');
		}
		if (i == 3)
		{
			ft_putchar(res[i] + '0');
			ft_putchar('\n');
		}
		i++;
	}
}

int		main(int argc, char **argv)
{
	int		i;
	char	tab[16];
	char	res[4][4];

	if (argc != 2 || check_argv(argv[1]) == 1)
	{
		write(1, "Error\n", 6);
		return (1);
	}
	i = 0;
	while (i < 31)
	{
		tab[i / 2] = argv[1][i] - '0';
		i += 2;
	}
	if (valid(res, 0, tab) == 0)
	{
		i = 0;
		while (i < 4)
			ft_print(res[i++]);
	}
	else
		write(1, "Error\n", 6);
	return (0);
}
