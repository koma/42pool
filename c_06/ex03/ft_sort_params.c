/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/06 00:27:29 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/07 12:29:13 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_sort_params(int argc, char **argv)
{
	int		i;
	int		j;
	int		k;
	char	*swap;

	i = 0;
	j = 1;
	while (j < argc)
	{
		k = j + 1;
		while (k < argc)
		{
			while (argv[j][i] == argv[k][i])
				i++;
			if (argv[j][i] > argv[k][i])
			{
				swap = argv[j];
				argv[j] = argv[k];
				argv[k] = swap;
			}
			k++;
			i = 0;
		}
		j++;
	}
}

int		main(int argc, char **argv)
{
	int		i;
	int		j;

	ft_sort_params(argc, argv);
	i = 1;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j])
		{
			write(1, &argv[i][j], 1);
			j++;
		}
		write(1, "\n", 1);
		i++;
	}
	return (0);
}
