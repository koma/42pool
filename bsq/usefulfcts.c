/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usefulfcts.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 15:46:24 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/18 23:29:43 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "usefulbsq.h"

int		is_in_charset(char c, char *charset)
{
	int i;

	i = 0;
	while (charset[i] && i < 2)
	{
		if (c == charset[i])
			return (1);
		i++;
	}
	return (0);
}

int		charset_malloc(char **charset, int argc)
{
	int i;

	i = 0;
	while (i < argc - 1)
	{
		if (!(charset[i] = (char *)malloc(sizeof(**charset) * 4)))
			return (0);
		charset[i][3] = 0;
		i++;
	}
	return (1);
}

int		*p_maps_malloc(char ****p_maps, char **maps, int argc)
{
	if (!(*p_maps = malloc(sizeof(**p_maps) * argc)))
		return (NULL);
	(*p_maps)[argc - 1] = 0;
	return (lines_length(maps, argc));
}

void	copy_map(char **map, char **copy, int lines_len, int nb_lines)
{
	int i;
	int j;

	i = 0;
	while (i < nb_lines)
	{
		j = 0;
		while (j < lines_len)
		{
			copy[i][j] = map[i][j];
			j++;
		}
		i++;
	}
}

char	*ft_strcat(char *s1, char *s2)
{
	int i;
	int j;
	char *output;

	i = 0;
	j = 0;
	if (s1 != 0)
	{
		while (s1[i])
			i++;
	}
	while (s2[j])
		j++;
	if (!(output = malloc(sizeof(*output) * (i + j + 1))))
		return (NULL);
	i = 0;
	if (s1 != 0)
	{
		while (s1[i])
		{
			output[i] = s1[i];
			i++;
		}
	}
	j = 0;
	while (s2[j])
	{
		output[i] = s2[j];
		i++;
		j++;
	}
	output[i] = 0;
	return (output);
}
