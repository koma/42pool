/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_maps.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoulin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 19:18:03 by mmoulin           #+#    #+#             */
/*   Updated: 2019/09/17 21:00:55 by mmoulin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "usefulbsq.h"

int		firstline_condition(char **maps, int i, int j)
{
	if (maps[i][j] <= 126 && maps[i][j] >= 32
		&& maps[i][j + 1] <= 126 && maps[i][j + 1] >= 32
		&& maps[i][j + 2] <= 126 && maps[i][j + 2] >= 32
		&& maps[i][j + 3] == 10
		&& maps[i][j] != maps[i][j + 1]
		&& maps[i][j] != maps[i][j + 2]
		&& maps[i][j + 1] != maps[i][j + 2])
		return (0);
	else
		return (-1);
}

int		check_first_line(char **maps, int i, int **nb_lines, char ***charset)
{
	int j;

	j = 0;
	(*nb_lines)[i] = 0;
	if (maps[i][j] <= 57 && maps[i][j] >= 48)
	{
		while (maps[i][j] <= 57 && maps[i][j] >= 48)
		{
			(*nb_lines)[i] = (*nb_lines)[i] * 10 + (maps[i][j] - 48);
			j++;
		}
	}
	else
		return (0);
	if (firstline_condition(maps, i, j) == 0)
	{
		(*charset)[i][0] = maps[i][j];
		(*charset)[i][1] = maps[i][j + 1];
		(*charset)[i][2] = maps[i][j + 2];
		j += 4;
	}
	else
		return (0);
	return (j);
}

int		check_grid(char **maps, int i, int j, char ***charset)
{
	int		var[3];

	L = 0;
	while (maps[i][j])
	{
		K = 0;
		if (is_in_charset(maps[i][j], (*charset)[i]))
		{
			while (is_in_charset(maps[i][j], (*charset)[i]))
			{
				K++;
				j++;
			}
		}
		else
			return (0);
		if ((L != 0 && K != MEM) || maps[i][j] != 10)
			return (0);
		else
			MEM = K;
		L++;
		j++;
	}
	return (L);
}

int		check_maps(char **maps, int **nb_lines, char ***charset)
{
	int		i;
	int		j;

	i = 0;
	while (maps[i])
	{
		if ((j = check_first_line(maps, i, nb_lines, charset)) == 0)
			return (0);
		if (check_grid(maps, i, j, charset) != (*nb_lines)[i])
			return (0);
		i++;
	}
	return (1);
}
