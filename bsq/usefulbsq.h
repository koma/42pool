/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usefulbsq.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoulin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 11:06:41 by mmoulin           #+#    #+#             */
/*   Updated: 2019/09/18 23:15:51 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef USEFULBSQ_H
# define USEFULBSQ_H

# include <unistd.h>

# include <stdlib.h>

# include <fcntl.h>

# define Y tab[0]
# define X tab[1]
# define NB_LINES tab[2]
# define LINE_LEN tab[3]

# define POS tab2[0]
# define SIZEMAX tab2[1]
# define NEWSIZE tab2[2]
# define M tab2[3]

# define L var[0]
# define K var[1]
# define MEM var[2]

# define I tab3[0]
# define J tab3[1]
# define A tab3[2]
# define B tab3[3]

# define MPOS pos % line_len
# define DPOS pos / line_len

char	*read_file(int fd, char *map);
int		copy_files(int argc, char **argv, char ***maps);
char	*ft_strcat(char *, char *);
int		is_in_charset(char c, char *charset);
void	algo(char **map, char *charset, int nb_lines, int line_len);
int		check_maps(char **maps, int **nb_lines, char ***charset);
int		charset_malloc(char **charset, int argc);
int		*p_maps_malloc(char ****p_maps, char **maps, int argc);
int		*lines_length(char **maps, int argc);
int		*algo2(char **map, char *charset, int nb_lines, int line_len);
void	print_map(char **map, int *posvalue, int mapsize[2], char *charset);
void	copy_map(char **map, char **copy, int line_len, int nb_lines);

#endif
