/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 15:28:53 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/18 23:38:01 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "usefulbsq.h"

int		copy_files(int argc, char **argv, char ***maps)
{
	int		i;
	int		fd;

	i = 0;
	while (i < argc - 1)
	{
		if ((fd = open(argv[i + 1], O_RDONLY)) == -1)
			return (0);
		(*maps)[i] = read_file(fd, (*maps)[i]);
		close(fd);
		i++;
	}
	(*maps)[i] = 0;
	return (1);
}

char	*read_file(int fd, char *map)
{
	char	buf[4097];
	int		size;

	map = NULL;
	while ((size = read(fd, buf, 4096)) == 4096)
	{
		buf[4096] = '\0';
		if (!(map = ft_strcat(map, buf)))
			return (NULL);
	}
	buf[size] = '\0';
	if (!(map = ft_strcat(map, buf)))
		return (NULL);
	close(fd);
	return (map);
}
