/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmoulin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 11:00:33 by mmoulin           #+#    #+#             */
/*   Updated: 2019/09/18 23:34:05 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "usefulbsq.h"

int		*lines_length(char **maps, int argc)
{
	int		i;
	int		j;
	int		k;
	int		*lines_len;

	i = 0;
	if (!(lines_len = malloc(sizeof(int) * argc)))
		return (NULL);
	while (i < argc - 1)
	{
		j = 0;
		while (maps[i][j] != '\n')
			j++;
		j++;
		k = 0;
		while (maps[i][j] != '\n')
		{
			j++;
			k++;
		}
		lines_len[i] = k;
		i++;
	}
	lines_len[argc - 1] = 0;
	return (lines_len);
}

int		copy_line(char ***p_maps, int *lines_len, char **maps, int tab3[4])
{
	B = 0;
	if (!(p_maps[I][A] = malloc(sizeof(char) * (lines_len[I] + 1))))
		return (0);
	p_maps[I][A][lines_len[I]] = 0;
	while (maps[I][J] != '\n')
		p_maps[I][A][B++] = maps[I][J++];
	A++;
	J++;
	return (1);
}

char	***parsing(char **maps, int argc, int *nb_lines)
{
	int		tab3[4];
	char	***p_maps;
	int		*lines_len;

	if ((lines_len = p_maps_malloc(&p_maps, maps, argc)) == 0)
		return (NULL);
	I = 0;
	while (I < argc - 1)
	{
		if (!(p_maps[I] = malloc(sizeof(**p_maps) * (nb_lines[I] + 1))))
			return (NULL);
		p_maps[I][nb_lines[I]] = 0;
		J = 0;
		A = 0;
		while (maps[I][J] != '\n')
			J++;
		J++;
		while (A < nb_lines[I])
		{
			if ((copy_line(p_maps, lines_len, maps, tab3)) == 0)
				return (0);
		}
		I++;
	}
	return (p_maps);
}

int		prealgo(char ***p_maps, char **charset, int *nb_lines, int *lines_len)
{
	int		i;
	int		j;
	int		mapsize[2];
	int		*posvalue;
	char	**copiedmap;

	i = -1;
	while (p_maps[++i])
	{
		if (!(copiedmap = malloc(sizeof(*copiedmap) * nb_lines[i])))
			return (0);
		j = 0;
		while (j < nb_lines[i])
		{
			if (!(copiedmap[j] = malloc(sizeof(char) * lines_len[i])))
				return (0);
			j++;
		}
		mapsize[0] = lines_len[i];
		mapsize[1] = nb_lines[i];
		copy_map(p_maps[i], copiedmap, lines_len[i], nb_lines[i]);
		posvalue = algo2(copiedmap, charset[i], nb_lines[i], lines_len[i]);
		print_map(p_maps[i], posvalue, mapsize, charset[i]);
	}
	return (1);
}

int		main(int argc, char **argv)
{
	char	**maps;
	char	***p_maps;
	int		*nb_lines;
	char	**charset;

	if (!(maps = malloc(sizeof(*maps) * (argc == 1 ? 2 : argc))))
		return (0);
	if (argc > 1)
	{
		if (!(copy_files(argc, argv, &maps)))
			return (0);
	}
	else if (argc == 1)
	{
		argc = 2;
		maps[0] = read_file(0, maps[0]);
	}
	if (!(nb_lines = (int *)malloc(sizeof(*nb_lines) * argc)))
		return (0);
	nb_lines[argc - 1] = 0;
	if (!(charset = (char **)malloc(sizeof(*charset) * argc)))
		return (0);
	charset[argc - 1] = 0;
	if (charset_malloc(charset, argc) == 0)
		return (0);
	if (!(check_maps(maps, &nb_lines, &charset)))
		return (0);
	p_maps = parsing(maps, argc, nb_lines);
	prealgo(p_maps, charset, nb_lines, lines_length(maps, argc));
}
