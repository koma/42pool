/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 14:14:57 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/18 20:39:09 by mmoulin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

#include "usefulbsq.h"

int		check_topleft(char **map, int y, int x)
{
	int values[3];

	values[0] = (int)map[y - 1][x - 1];
	values[1] = (int)map[y - 1][x];
	values[2] = (int)map[y][x - 1];
	if (values[0] <= values[1] && values[0] <= values[2])
		return (values[0] + 1);
	else if (values[1] <= values[0] && values[1] <= values[2])
		return (values[1] + 1);
	else
		return (values[2] + 1);
}

void	algo_core(char **map, int line_len, int *mem, int pos)
{
	if (MPOS == 0 || DPOS == 0)
	{
		map[DPOS][MPOS] = 1;
		if (mem[1] == 0)
		{
			mem[0] = pos;
			mem[1] = 1;
		}
	}
	else
	{
		map[DPOS][MPOS] = (char)check_topleft(map, DPOS, MPOS);
		if (mem[1] < (int)check_topleft(map, DPOS, MPOS))
		{
			mem[0] = pos;
			mem[1] = check_topleft(map, DPOS, MPOS);
		}
	}
}

int		*algo2(char **map, char *charset, int nb_lines, int line_len)
{
	int	pos;
	int *mem;

	if (!(mem = malloc(sizeof(*mem) * 2)))
		return (NULL);
	pos = -1;
	while (++pos < nb_lines * line_len)
	{
		if (map[DPOS][MPOS] == charset[0])
			algo_core(map, line_len, mem, pos);
		else
			map[DPOS][MPOS] = 0;
	}
	return (mem);
}

void	print_map(char **map, int *res, int mapsize[2], char *charset)
{
	int i;
	int j;

	i = 0;
	while (i < res[1])
	{
		j = 0;
		while (j < res[1])
		{
			map[res[0] / mapsize[0] - i][res[0] % mapsize[0] - j] = charset[2];
			j++;
		}
		i++;
	}
	i = -1;
	while (++i < mapsize[1])
	{
		j = 0;
		while (j < mapsize[0])
		{
			write(1, &(map[i][j]), 1);
			j++;
		}
		write(1, "\n", 1);
	}
}
