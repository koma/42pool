/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 16:01:50 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/08/29 18:29:54 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr_while(int nb, int n, int p, int neg)
{
	int		nbtabrev[10];
	char	nbtab[10];
	int		i;

	i = 0;
	while (nb > 0)
	{
		nbtabrev[--p] = nb % 10;
		nb /= 10;
		n++;
	}
	while (p < 10)
	{
		nbtab[i++] = nbtabrev[p++] + '0';
	}
	if (neg == 1)
		write(1, "-", 1);
	write(1, nbtab, n);
}

void	ft_putnbr_if(int nb, int n, int p)
{
	int neg;

	neg = 0;
	if (nb < 0)
	{
		nb -= nb * 2;
		neg = 1;
	}
	if (nb == -2147483648)
		write(1, "-2147483648", 11);
	else if (nb == 0)
		write(1, "0", 1);
	else
	{
		ft_putnbr_while(nb, n, p, neg);
	}
}

void	ft_putnbr(int nb)
{
	int n;
	int p;

	n = 0;
	p = 10;
	ft_putnbr_if(nb, n, p);
}
