/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 21:12:55 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/19 21:25:20 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list last_elem;
	t_list *begin_list_copy;

	begin_list_copy = *begin_list;
	while (begin_list_copy->next)
		begin_list_copy = begin_list_copy->next;
	begin_list_copy = &last_elem;
	last_elem.data = data;
	last_elem.next = NULL;
}
