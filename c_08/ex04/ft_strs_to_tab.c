/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strs_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 16:54:19 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/11 14:58:29 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_str.h"

struct s_stock_str	*ft_strs_to_tab(int ac, char **av)
{
	t_stock_str	*structtab;
	int			i;
	int			j;

	if (!(structtab = malloc(sizeof(*structtab) * (ac + 1))))
		return (0);
	j = 0;
	while (j < ac)
	{
		i = 0;
		while (av[j][i])
			i++;
		structtab[j].size = i;
		if (!(structtab[j].copy = malloc(sizeof(*av[j]) * i + 1)))
			return (0);
		structtab[j].str = av[j];
		i = -1;
		while (av[j][++i])
			structtab[j].copy[i] = av[j][i];
		structtab[j].copy[i] = 0;
		j++;
	}
	structtab[ac].str = 0;
	return (structtab);
}
