#include <stdio.h>
#include <stdlib.h>

int main(int ac, char *av[])
{
	printf("%d / %d = %d\n", atoi(av[1]), atoi(av[2]), atoi(av[1]) / atoi(av[2]));
	printf("%d %% %d = %d\n", atoi(av[1]), atoi(av[2]), atoi(av[1]) % atoi(av[2]));
	return (0);
}
