#!/bin/sh
cd $(git rev-parse --show-toplevel) && find . -type f -exec basename \{\} \; | git check-ignore --stdin
