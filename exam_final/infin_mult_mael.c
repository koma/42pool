#include <unistd.h>
#include <stdlib.h>

int     ft_strlen(char *str)
{
        int i;

        i = 0;
        while (str[i])
                i++;
        return (i);
}
//#include <stdio.h>
char    digit(char *nbr1, char *nbr2, int *lengths, int index, int *retenue)
{
        int j = 0;
        int k;
        int result;

        result = *retenue;
		//printf("index = %d | retenue = %d\n", index, *retenue);
        while (j < lengths[1] && j <= index)
        {
                k = 0;
                while (k < lengths[2] && k <= index)
                {
                        if (j + k == index)
						{
                                result += (int)(nbr1[lengths[1] - 1 - j] - 48) * (int)(nbr2[lengths[2] - 1 - k] - 48);
								//printf("%d * %d + %d = %d\n", nbr1[lengths[1] - 1 - j] - 48, nbr2[lengths[2] - 1 - k] - 48, *retenue, result);
						}
                        k++;
                }
                j++;
        }
        *retenue = result / 10;
        result = result % 10;
        return ((char)(result + 48));
}

int     main(int argc, char **argv)
{
        char *res;
        int i = -1;
        int j;
        int lengths[3];
        int retenue = 0;
        int *ptr;

        if (argv[1][0] == '-' && argv[2][0] != '-')
        {
                write(1, "-", 1);
                argv[1][0] = '0';
        }
        else if (argv[1][0] != '-' && argv[2][0] == '-')
        {
                write(1, "-", 1);
                argv[2][0] = '0';
        }
        else if (argv[1][0] == '-' && argv[2][0] == '-')
        {
                argv[1][0] = '0';
                argv[2][0] = '0';
        }
        lengths[1] = ft_strlen(argv[1]);
        lengths[2] = ft_strlen(argv[2]);
        lengths[0] = lengths[1] + lengths[2];
        ptr = &retenue;
        if (!(res = malloc(sizeof(char) * (lengths[0] + 1))))
                return (1);
        res[lengths[0]] = 0;
        while (++i < lengths[0])
                res[lengths[0] - i - 1] = digit(argv[1], argv[2], lengths, i, ptr);
        i = 0;
        j = 0;
        while (res[i] == '0')
                i++;
        while (res[i])
        {
                res[j] = res[i];
                j++;
                i++;
        }
        res[j] = 0;
        i = -1;
        while (res[++i])
                write(1, res + i, 1);
        write(1, "\n", 1);
        free(res);
        return (1);
}
