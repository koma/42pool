/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buzzfizz.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 10:16:31 by exam              #+#    #+#             */
/*   Updated: 2019/09/20 10:24:37 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int nb)
{
	char c;

	if (nb > 9)
		ft_putnbr(nb / 10);
	c = nb % 10 + '0';
	write(1, &c, 1);
}

int		main(void)
{
	int		nb;

	nb = 1;
	while (nb <= 100)
	{
		if (nb % 4 == 0)
			write(1, "buzz", 4);
		if (nb % 7 == 0)
			write(1, "fizz", 4);
		if (nb % 4 != 0 && nb % 7 != 0)
			ft_putnbr(nb);
		write(1, "\n", 1);
		nb++;
	}
	return (0);
}
