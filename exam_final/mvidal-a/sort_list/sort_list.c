/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 12:38:04 by exam              #+#    #+#             */
/*   Updated: 2019/09/20 15:00:11 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_list	*sort_list(t_list* lst, int (*cmp)(int, int))
{
	t_list	*latter;
	t_list	*former;
	t_list	*tmp_ptr;
	t_list	tmp;

	former = lst;
	while (former)
	{
		latter = former->next;
		while (latter)
		{
			if (!cmp(former->data, latter->data))
			{
				tmp_ptr = latter->next;
				latter->next = former->next;
				former->next = tmp_ptr;
				tmp = *former;
				*former = *latter;
				*latter = tmp;
			}
			latter = latter->next;
		}
		former = former->next;
	}
	return (lst);
}
