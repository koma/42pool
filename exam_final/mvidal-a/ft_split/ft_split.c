/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 12:02:30 by exam              #+#    #+#             */
/*   Updated: 2019/09/20 12:34:25 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_wordlen(char *str)
{
	int		i;

	i = 0;
	while (str[i] && str[i] != ' ' && str[i] != '\n' && str[i] != '\t')
		i++;
	return (i);
}

int		ft_wordcount(char *str)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	k = 0;
	while (str[i])
	{
		if (i + (j = ft_wordlen(&str[i])) > i)
		{
			i += j;
			k++;
		}
		else
			i++;
	}
	return (k);
}

char	**ft_split(char *str)
{
	char	**strs;
	int		i;
	int		j;
	int		k;
	int		l;
	int		m;

	k = ft_wordcount(str);
	if (!(strs = (char **)malloc(sizeof(*strs) * (k + 1))))
		return (NULL);
	strs[k] = NULL;
	l = 0;
	m = 0;
	while (l < k)
	{
		if ((j = ft_wordlen(&str[m])) > 0)
		{
			if (!(strs[l] = (char *)malloc(sizeof(**strs) * (j + 1))))
				return (NULL);
			strs[l][j] = 0;
			i = 0;
			while (i < j)
				strs[l][i++] = str[m++];
			l++;
		}
		else
			m++;
	}
	return (strs);
}
