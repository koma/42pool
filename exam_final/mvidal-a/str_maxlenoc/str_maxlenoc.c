/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_maxlenoc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 15:39:57 by exam              #+#    #+#             */
/*   Updated: 2019/09/20 17:26:34 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

typedef	struct	s_found
{
	int		n;
	char	*str;
}				t_f;

void	ft_putstr(char *str)
{
	while (*str)
		write(1, str++, 1);
}

int		search_letter(char to_find, char **strs)
{
	int		i;
	int		j;
	int		n;

	i = 2;
	n = 0;
	while (strs[i])
	{
		j = 0;
		while (strs[i][j])
		{
			if (to_find == strs[i][j])
			{
				n++;
				break ;
			}
			j++;
		}
		i++;
	}
	return (n);
}

int		search_string(char *to_find, char **strs)
{
	int		i;
	int		j;
	int		k;
	int		n;
	int		m;

	i = 2;
	n = 0;
	k = 0;
	while (strs[i])
	{
		j = 0;
		while (strs[i][j])
		{
			m = j;
			k = 0;
			while (to_find[k] && to_find[k] == strs[i][j])
			{
				k++;
				j++;
			}
			if (!to_find[k])
			{
				n++;
				break ;
			}
			j = m + 1;
		}
		i++;
	}
	return (n);
}

int		ft_strlen(char *str)
{
	int		i;
	
	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strncpy(char *src, int s, char *old)
{
	char	*str;
	int		i;

	free(old);
	if (!(str = (char *)malloc(sizeof(*str) * (s + 1))))
		return (NULL);
	str[s] = 0;
	i = 0;
	while (i < s)
	{
		str[i] = src[i];
		i++;
	}
	return (str);
}

int		main(int argc, char **argv)
{
	char	*str;
	int		i;
	int		j;
	int		s;
	int		n;
	t_f		found;

	if (argc > 2)
	{
		i = 0;
		if (!(str = (char *)malloc(sizeof(*str) * 1)))
			return (0);
		str[0] = 0;
		found.n = 0;
		while (argv[1][i])
		{
			if (search_letter(argv[1][i], argv) == argc - 2)
			{
				n = 1;
				j = 2;
				s = ft_strlen(&argv[1][i]);
				while (j <= s)
				{
					str = ft_strncpy(&argv[1][i], j, str);
					if (search_string(str, argv) == argc - 2)
						n++;
					j++;
				}
				if (n > found.n)
				{
					found.n = n;
					found.str = &argv[1][i];
				}
			}
			i++;
		}
		free(str);
		if (found.n > 0)
			write(1, found.str, found.n);
	}
	else if (argc == 2)
		write(1, argv[1], ft_strlen(argv[1]));
	write(1, "\n", 1);
	return (0);
}
