/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rrange.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 11:41:32 by exam              #+#    #+#             */
/*   Updated: 2019/09/20 12:00:13 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_rrange(int start, int end)
{
	int		i;
	int		s;
	int		de_cres;
	int		*tab;

	if (start - end >= 0)
	{
		s = start - end + 1;
		de_cres = 1;
	}
	else
	{
		s = end - start + 1;
		de_cres = -1;
	}
	if (!(tab = (int *)malloc(sizeof(*tab) * s)))
		return (NULL);
	i = 0;
	while (i < s)
	{
		tab[i] = end;
		end += de_cres;
		i++;
	}
	return (tab);
}
