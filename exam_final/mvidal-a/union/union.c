/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   union.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 11:14:55 by exam              #+#    #+#             */
/*   Updated: 2019/09/20 11:39:48 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		main(int argc, char **argv)
{
	int i;
	int j;
	int a1_s;

	i = 0;
	a1_s = 0;
	if (argc == 3)
	{
		while (argv[1][i])
		{
			j = 0;
			while (j < i && argv[1][i] != argv[1][j])
				j++;
			if (j == i)
				write(1, &argv[1][i], 1);
			i++;
		}
		a1_s = i;
		i = 0;
		while (argv[2][i])
		{
			j = 0;
			while (argv[1][j] && argv[2][i] != argv[1][j])
				j++;
			if (j == a1_s)
			{
				j = 0;
				while (j < i && argv[2][i] != argv[2][j])
					j++;
				if (j == i)
					write(1, &argv[2][i], 1);
			}
			i++;
		}
	}
	write(1, "\n", 1);
	return (0);
}
