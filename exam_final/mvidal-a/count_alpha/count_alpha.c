/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_alpha.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/20 15:03:29 by exam              #+#    #+#             */
/*   Updated: 2019/09/20 15:35:48 by exam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int nb)
{
	char c;

	if (nb > 9)
		ft_putnbr(nb / 10);
	c = nb % 10 + '0';
	write(1, &c, 1);
}

int		search_and_eliminate(char *str)
{
	int i;
	int n;

	i = 1;
	n = 1;
	if (str[0] >= 'A' && str[0] <= 'Z')
		str[0] += 32;
	while (str[i])
	{
		if (str[0] == str[i] || str[0] == str[i] + 32)
		{
			n++;
			str[i] = '0';
		}
		i++;
	}
	return (n);
}

int		main(int argc, char **argv)
{
	int i;

	if (argc == 2)
	{
		while (*argv[1] && (*argv[1] < 'A' || (*argv[1] > 'Z' && *argv[1] < 'a') || *argv[1] > 'z'))
			argv[1]++;
		i = 0;
		while (argv[1][i])
		{
			if ((argv[1][i] >= 'A' && argv[1][i] <= 'Z') || (argv[1][i] >= 'a' && argv[1][i] <= 'z'))
			{
				if (i != 0)
					write(1, ", ", 2);
				ft_putnbr(search_and_eliminate(&argv[1][i]));
				write(1, &argv[1][i], 1);
			}
			i++;
		}
	}
	write(1, "\n", 1);
	return (0);
}
