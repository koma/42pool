/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 11:13:27 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/01 20:45:10 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	rush_x(int x, char a, char b, char c)
{
	int i;

	i = 0;
	ft_putchar(a);
	while (i < x - 2)
	{
		ft_putchar(b);
		i++;
	}
	if (x > 1)
		ft_putchar(c);
	ft_putchar('\n');
}

void	rush(int x, int y)
{
	int j;

	if (x > 0 && y > 0)
	{
		rush_x(x, 'o', '-', 'o');
		j = 0;
		while (j < y - 2)
		{
			rush_x(x, '|', ' ', '|');
			j++;
		}
		if (y > 1)
			rush_x(x, 'o', '-', 'o');
	}
}
