/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gigregoi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 15:39:53 by gigregoi          #+#    #+#             */
/*   Updated: 2019/09/01 20:39:53 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);
void	rush_x(int x, char a, char b, char c);
void	rush_y(int x, int y, char *n);
void	rush(int x, int y, int n);

int		main(void)
{
	rush(-8, -9, -4);
	return (0);
}
