/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 11:13:27 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/01 20:44:25 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	rush_x(int x, char a, char b, char c)
{
	int i;

	i = 0;
	ft_putchar(a);
	while (i < x - 2)
	{
		ft_putchar(b);
		i++;
	}
	if (x > 1)
		ft_putchar(c);
	ft_putchar('\n');
}

void	rush_y(int x, int y, char *n)
{
	int j;

	rush_x(x, n[0], n[1], n[2]);
	j = 0;
	while (j < y - 2)
	{
		rush_x(x, n[3], ' ', n[3]);
		j++;
	}
	if (y > 1)
		rush_x(x, n[4], n[1], n[5]);
}

void	rush(int x, int y, int n)
{
	if (x > 0 && y > 0 && n >= 0 && n <= 4)
	{
		if (n == 0)
			rush_y(x, y, "o-o|oo");
		else if (n == 1)
			rush_y(x, y, "/*\\*\\/");
		else if (n == 2)
			rush_y(x, y, "ABABCC");
		else if (n == 3)
			rush_y(x, y, "ABCBAC");
		else if (n == 4)
			rush_y(x, y, "ABCBCA");
	}
	else
	{
		write(1, "Veuillez entrer deux nombres entiers positifs, \
puis un numero de rush compris entre 0 et 4.\n", 92);
	}
}
