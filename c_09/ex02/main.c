#include <stdio.h>

char	**ft_split(char *str, char *charset);

int	main()
{
	char str[] = ",  |h moon,,goodbye|hearth|  ,";
	char charset[] = "|| ,";
	char **strs;

	strs = ft_split(str, charset);
	int i = 0;
	while (strs[i])
	{
		printf("%s\n", strs[i]);
		i++;
	}
}
