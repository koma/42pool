#include <string.h>
#include <stdio.h>

int	ft_strcmp(char *s1, char *s2);

int main()
{
	char s1[] = "hello";
	char s2[] = "hello";
	s2[3] = -15;

	printf("%s\n%s\n", s1, s2);
	printf("%d\n", strcmp(s1, s2));
	printf("%d\n", ft_strcmp(s1, s2));
}
