/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 12:33:52 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/19 11:29:52 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int i;
	int j;
	int n;

	if (!to_find[0])
		return (str);
	i = 0;
	n = 0;
	while (str[i])
	{
		j = 0;
		while (str[i] != to_find[j] && str[i])
			i++;
		if (str[i] == 0)
			break ;
		if (str[i] == to_find[j])
			n = i;
		while (str[i++] == to_find[j] && to_find[j])
			j++;
		if (!to_find[j])
			return (str + n);
		i = n + 1;
	}
	return (0);
}
