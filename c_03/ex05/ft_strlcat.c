/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 14:59:58 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/17 11:09:44 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned	i;
	int			j;
	unsigned	k;
	int			l;

	i = 0;
	while (dest[i])
		i++;
	k = i;
	l = 0;
	while (src[l])
		l++;
	j = 0;
	while (src[j] && i < size - 1 && size)
		dest[i++] = src[j++];
	if (size > k)
	{
		dest[i] = 0;
		return (l + k);
	}
	else
		return (l + size);
}
