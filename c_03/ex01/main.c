#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int	ft_strncmp(char *s1, char *s2, unsigned int n);

int main(int ac, char *av[])
{
	char *s1;
	char *s2;
	unsigned int n;
	s1 = av[1];
	s2 = av[2];
	n = atoi(av[3]);

	ac -= 1;
	printf("%s\n%s\n", s1, s2);
	printf("%d\n", strncmp(s1, s2, n));
	printf("%d\n", ft_strncmp(s1, s2, n));
}
