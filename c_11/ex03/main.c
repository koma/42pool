#include <stdio.h>
int		ft_count_if(char **tab, int length, int (*f)(char*));

int		ft_begins_with_minus(char *str)
{
	return (str[0] == '-' ? 1 : 0);
}

int		main()
{
	char *strs[4] = {"-1", "-1", "-2", "3"};
	printf("%d", ft_count_if(strs, 4, &ft_begins_with_minus));
}
