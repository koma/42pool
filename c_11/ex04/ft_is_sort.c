/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 14:51:06 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/19 17:35:12 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	int i;
	int c;

	i = 0;
	c = 0;
	while (i + 1 < length && c == 0)
	{
		if ((*f)(tab[i], tab[i + 1]) > 0)
			c++;
		i++;
	}
	while (i + 1 < length && c == 1)
	{
		if ((*f)(tab[i], tab[i + 1]) < 0)
			c++;
		i++;
	}
	return (c < 2 ? 1 : 0);
}
