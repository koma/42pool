#include <stdio.h>
int		ft_is_sort(int *tab, int length, int(*f)(int, int));

int		ft_compare(int i, int j)
{
	return (i - j);
}

int		main()
{
	int tab[4] = {2, 3, 2, 1};
	printf("%d", ft_is_sort(tab, 4, &ft_compare));
}
