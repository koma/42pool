/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ops.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 18:07:26 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/19 19:09:08 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		plus(int i, int j)
{
	return (i + j);
}

int		minus(int i, int j)
{
	return (i - j);
}

int		multiply(int i, int j)
{
	return (i * j);
}

int		divide(int i, int j)
{
	return (i / j);
}

int		modulo(int i, int j)
{
	return (i % j);
}
