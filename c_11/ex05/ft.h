/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 18:05:40 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/19 18:08:07 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H

#include <unistd.h>
typedef struct	s_op
{
	char op;
	int (*f)(int, int);
}				t_op;

void	ft_putnbr(int nb);
int		ft_atoi(char *str);

int		plus(int i, int j);
int		minus(int i, int j);
int		multiply(int i, int j);
int		divide(int i, int j);
int		modulo(int i, int j);

#endif
