/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_op.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 15:39:17 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/19 19:10:45 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	struct_init(t_op ops[5])
{
	ops[0].op = '+';
	ops[0].f = &plus;
	ops[1].op = '-';
	ops[1].f = &minus;
	ops[2].op = '*';
	ops[2].f = &multiply;
	ops[3].op = '/';
	ops[3].f = &divide;
	ops[4].op = '%';
	ops[4].f = &modulo;
}

void	ft_stop_div_mod(char op)
{
	if (op == '/')
		write(1, "Stop : division by zero\n", 24);
	if (op == '%')
		write(1, "Stop : modulo by zero\n", 22);
}

int		main(int argc, char **argv)
{
	t_op	ops[5];
	int		res;
	int		i;

	res = 0;
	struct_init(ops);
	if (argc != 4)
		return (0);
	if ((*argv[2] == '%' || *argv[2] == '/') && ft_atoi(argv[3]) == 0)
	{
		ft_stop_div_mod(*argv[2]);
		return (0);
	}
	i = -1;
	while (++i < 5)
	{
		if (ops[i].op == *argv[2])
		{
			res = ops[i].f(ft_atoi(argv[1]), ft_atoi(argv[3]));
			break ;
		}
	}
	ft_putnbr(res);
	write(1, "\n", 1);
	return (0);
}
