#include <stdio.h>
int		*ft_map(int *tab, int length, int(*f)(int));

int		ft_plus_one(int nb)
{
	return (nb + 1);
}

int		main()
{
	int	tab[4] = {41, 42, 43, 44};
	int i = 0;
	int *res;
	res = ft_map(tab, 4, &ft_plus_one);
	while (i < 4)
		printf("%d", res[i++]);
}
