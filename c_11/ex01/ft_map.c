/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvidal-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 11:56:26 by mvidal-a          #+#    #+#             */
/*   Updated: 2019/09/19 17:33:42 by mvidal-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_map(int *tab, int length, int (*f)(int))
{
	int		i;
	int		*ret;

	if (!(ret = (int *)malloc(sizeof(*ret) * length)))
		return (0);
	i = -1;
	while (++i < length)
		ret[i] = (*f)(tab[i]);
	return (ret);
}
