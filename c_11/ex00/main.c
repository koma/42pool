void	ft_putnbr(int nb);
void	ft_foreach(int *tab, int length, void(*f)(int));

int		main()
{
	int	tab[4] = {41, 42, 43, 44};
	ft_foreach(tab, 4, &ft_putnbr);
}
