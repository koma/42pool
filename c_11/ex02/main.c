#include <stdio.h>
int		ft_any(char **tab, int(*f)(char*));

int		ft_begins_with_minus(char *str)
{
	return (str[0] == '-' ? 1 : 0);
}

int		main()
{
	char *strs[4] = {"1", "1", "2"};
	strs[3] = 0;
	printf("%d", ft_any(strs, &ft_begins_with_minus));
}
